//公共的url地址
let baseUrl="http://api.douban.com/v2/movie/"
//进入网页的钥匙
let apikey="0df993c66c0c636e29ecbb5344252a4a"
//封装一个http请求
function http({
  //传递两个参数，一个是不同的url地址，一个是回调函数
    url,
    callback
  }){
   wx.request({
      data:{
        //问号后面的参数都可以放在data里面
        apikey
      },
      //将公共的baseurl与网页独立的url段结合
      url: baseUrl+url,
      dataType: 'json',
      header: {'content-type':'json'},
      method: 'GET',
      responseType:'text',
      success: (res) => {
        //触发回调函数，把请求数据的结果当作回调函数的实参传递出去
        callback(res)
      }
    })
  }

  //二次封装http请求
  function getTop250model(
    {
      //不用关注count,需要关注的是start
      start=0,
      //这里将回调函数作为参数给下面的http函数
      callback
    }
  ){
    http({
      callback,
      //这里是网页独立的url片段，就是上面传入的url，动态改变start
      url:`top250?start=${start}&count=20`
    })
  }
  //暴露出二次封装的函数
module.exports=getTop250model