const getTop250model=require("../../request/http")
Page({
  data: {
     banner:[{id:1001,url:"../../images/banner.jpg"},
     {id:1002,url:"../../images/banner.png"},
     {id:1003,url:"../../images/bg.jpg"},
     {id:1004,url:"../../images/timg.jpg"}
    ],
    arr:[],
    isplay:true
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {  
    let arr=[]
  wx.showLoading({
      title:"数据加载中" ,
    });
    getTop250model({
      callback:res=>{
        wx.hideLoading();
      let subjects=res.data.subjects
      subjects.forEach(item=>{
      let {rating,title,casts,id,images}=item;
         let stars = [];
        let rate = rating.average
        for (let i = 0; i < 5; i++) {
          if (rate >= 2) {
            stars.push(2)
          } else if (rate > 0) {
            stars.push(rate);
          } else {
            stars.push(0)
          }
          rate = rate - 2;
        }
      arr.push({
        id,
        title,
        score:rating.average,
        url:images.small,
        stars
      })
      this.setData({
        arr
      })
      })
      }
    })
  },
  
  onReachBottom:function(){
    let length = this.data.arr.length;
    getTop250model({
      start:length,
      callback:res=>{
        let arr1=[]
        let subjects=res.data.subjects
        subjects.forEach(item=>{
          let {rating,title,casts,id,images}=item;
          let stars = [];
          //这里的rate是请求的评分数据
          let rate = rating.average
          for (let i = 0; i < 5; i++) {
            if (rate >= 2) {
              stars.push(2)
            } else if (rate > 0) {
              stars.push(rate);
            } else {
              stars.push(0)
            }
            rate = rate - 2;
          }
          arr1.push({
            id,
            title,
            score:rating.average,
            url:images.small,
            stars
          })
        })
        let arr = this.data.arr.concat(arr1);
        this.setData({
          arr
        })
      }
    })
  },

  handleToggle(e){
    let {id} = e.currentTarget.dataset
 wx.navigateTo({
   url: `/pages/content/index?id=${id}`
 })
  },
  handleclick(){
    let isplay=!this.data.isplay
    this.setData({
      isplay
    })
  }
 
})