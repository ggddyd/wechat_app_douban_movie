Page({

  /**
   * 页面的初始数据
   */
  data: {
   arr:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options.id);
    wx.request({
      url: `http://api.douban.com/v2/movie/${options.id}?apikey=0df993c66c0c636e29ecbb5344252a4a`,
      //这里的id值时动态的
      data:{},
      header: {
      'Content-Type': 'json'
      },
      dataType: 'json',
      responseType: 'text',
      success:(res)=> {
      let{ summary,alt_title,image}=res.data
      let arr=[]
      arr.push({
        summary,
        alt_title,
        url:image
      })
      this.setData({
       arr
      })
      }
      })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})